plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
}

gradlePlugin {
    plugins {
        register("printPlugin") {
            id = "printPlugin"
            implementationClass = "com.example.plugin.PrintFiggletPlugin"
        }
    }
}

dependencies {
    implementation("com.github.lalyos:jfiglet:0.0.9")
}
