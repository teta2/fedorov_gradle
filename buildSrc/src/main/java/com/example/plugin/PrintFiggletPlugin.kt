package com.example.plugin

import com.github.lalyos.jfiglet.FigletFont
import org.gradle.api.Plugin
import org.gradle.api.Project

class PrintFiggletPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        val figgletTask = target.tasks.register("figglet") {
            group = "My tasks"
            doLast {
                val nice = FigletFont.convertOneLine("Hello, world")
                println(nice)
            }
        }
        target.tasks.whenTaskAdded {
            if (name == "assembleDebug") {
                finalizedBy(figgletTask)
            }
        }
    }
}